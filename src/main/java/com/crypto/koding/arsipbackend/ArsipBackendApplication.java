package com.crypto.koding.arsipbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArsipBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArsipBackendApplication.class, args);
	}

}
